﻿namespace USBHIDDriverEx
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUSBVID = new System.Windows.Forms.Label();
            this.lblUSBPID = new System.Windows.Forms.Label();
            this.tboxUSBVID = new System.Windows.Forms.TextBox();
            this.tboxUSBPID = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblList = new System.Windows.Forms.Label();
            this.lboxList = new System.Windows.Forms.ListBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lslSend = new System.Windows.Forms.Label();
            this.lblReceive = new System.Windows.Forms.Label();
            this.tboxSend = new System.Windows.Forms.TextBox();
            this.tboxReceive = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblUSBVID
            // 
            this.lblUSBVID.AutoSize = true;
            this.lblUSBVID.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUSBVID.Location = new System.Drawing.Point(15, 15);
            this.lblUSBVID.Name = "lblUSBVID";
            this.lblUSBVID.Size = new System.Drawing.Size(77, 18);
            this.lblUSBVID.TabIndex = 0;
            this.lblUSBVID.Text = "USB VID";
            // 
            // lblUSBPID
            // 
            this.lblUSBPID.AutoSize = true;
            this.lblUSBPID.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUSBPID.Location = new System.Drawing.Point(15, 45);
            this.lblUSBPID.Name = "lblUSBPID";
            this.lblUSBPID.Size = new System.Drawing.Size(76, 18);
            this.lblUSBPID.TabIndex = 1;
            this.lblUSBPID.Text = "USB PID";
            // 
            // tboxUSBVID
            // 
            this.tboxUSBVID.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxUSBVID.Location = new System.Drawing.Point(90, 15);
            this.tboxUSBVID.Name = "tboxUSBVID";
            this.tboxUSBVID.Size = new System.Drawing.Size(100, 27);
            this.tboxUSBVID.TabIndex = 2;
            // 
            // tboxUSBPID
            // 
            this.tboxUSBPID.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxUSBPID.Location = new System.Drawing.Point(90, 45);
            this.tboxUSBPID.Name = "tboxUSBPID";
            this.tboxUSBPID.Size = new System.Drawing.Size(100, 27);
            this.tboxUSBPID.TabIndex = 3;
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(644, 13);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(127, 23);
            this.btnConnect.TabIndex = 4;
            this.btnConnect.Text = "CONNECT";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblList
            // 
            this.lblList.AutoSize = true;
            this.lblList.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblList.Location = new System.Drawing.Point(15, 90);
            this.lblList.Name = "lblList";
            this.lblList.Size = new System.Drawing.Size(77, 18);
            this.lblList.TabIndex = 5;
            this.lblList.Text = "USB List";
            // 
            // lboxList
            // 
            this.lboxList.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lboxList.FormattingEnabled = true;
            this.lboxList.ItemHeight = 18;
            this.lboxList.Location = new System.Drawing.Point(90, 90);
            this.lboxList.Name = "lboxList";
            this.lboxList.Size = new System.Drawing.Size(548, 166);
            this.lboxList.TabIndex = 6;
            this.lboxList.SelectedIndexChanged += new System.EventHandler(this.lboxList_SelectedIndexChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(644, 88);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(127, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "SEARCH";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lslSend
            // 
            this.lslSend.AutoSize = true;
            this.lslSend.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lslSend.Location = new System.Drawing.Point(15, 275);
            this.lslSend.Name = "lslSend";
            this.lslSend.Size = new System.Drawing.Size(52, 18);
            this.lslSend.TabIndex = 8;
            this.lslSend.Text = "SEND";
            // 
            // lblReceive
            // 
            this.lblReceive.AutoSize = true;
            this.lblReceive.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceive.Location = new System.Drawing.Point(15, 340);
            this.lblReceive.Name = "lblReceive";
            this.lblReceive.Size = new System.Drawing.Size(78, 18);
            this.lblReceive.TabIndex = 9;
            this.lblReceive.Text = "RECEIVE";
            // 
            // tboxSend
            // 
            this.tboxSend.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxSend.Location = new System.Drawing.Point(15, 300);
            this.tboxSend.Name = "tboxSend";
            this.tboxSend.Size = new System.Drawing.Size(623, 27);
            this.tboxSend.TabIndex = 10;
            // 
            // tboxReceive
            // 
            this.tboxReceive.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxReceive.Location = new System.Drawing.Point(15, 365);
            this.tboxReceive.Name = "tboxReceive";
            this.tboxReceive.Size = new System.Drawing.Size(623, 27);
            this.tboxReceive.TabIndex = 11;
            // 
            // btnSend
            // 
            this.btnSend.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.Location = new System.Drawing.Point(644, 301);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(127, 23);
            this.btnSend.TabIndex = 12;
            this.btnSend.Text = "SEND";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisconnect.Location = new System.Drawing.Point(644, 46);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(127, 23);
            this.btnDisconnect.TabIndex = 13;
            this.btnDisconnect.Text = "DISCONNECT";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 455);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.tboxReceive);
            this.Controls.Add(this.tboxSend);
            this.Controls.Add(this.lblReceive);
            this.Controls.Add(this.lslSend);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lboxList);
            this.Controls.Add(this.lblList);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.tboxUSBPID);
            this.Controls.Add(this.tboxUSBVID);
            this.Controls.Add(this.lblUSBPID);
            this.Controls.Add(this.lblUSBVID);
            this.Name = "frmMain";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUSBVID;
        private System.Windows.Forms.Label lblUSBPID;
        private System.Windows.Forms.TextBox tboxUSBVID;
        private System.Windows.Forms.TextBox tboxUSBPID;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblList;
        private System.Windows.Forms.ListBox lboxList;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lslSend;
        private System.Windows.Forms.Label lblReceive;
        private System.Windows.Forms.TextBox tboxSend;
        private System.Windows.Forms.TextBox tboxReceive;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnDisconnect;
    }
}