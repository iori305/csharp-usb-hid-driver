﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using USBHIDDRIVER;

namespace USBHIDDriverEx
{
    public partial class frmMain : Form
    {

        USBHIDDRIVER.USBInterface usbI;// = new USBInterface("vid_0000", "pid_0000");

        public frmMain()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            tboxUSBPID.Text = "";
            tboxUSBVID.Text = "";

            tboxSend.MaxLength = 256;
            
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (tboxUSBVID.Text == "" && tboxUSBPID.Text == "")
            {
                MessageBox.Show("Please Enter VID and PID");
            }
            if (tboxUSBPID.TextLength != 4 || tboxUSBVID.TextLength != 4)
            {
                MessageBox.Show("VID or PID Length Error");
            }

            usbI = new USBInterface("vid_" + tboxUSBVID.Text.ToLower(), "pid_" + tboxUSBPID.Text.ToLower());

            if (usbI.Connect())
            {
                MessageBox.Show("Connect Success");
                btnConnect.Enabled = false;
                btnDisconnect.Enabled = true;
                btnSend.Enabled = true;
            }
            else
            {
                MessageBox.Show("Connect Fail");
                btnConnect.Enabled = true;
                btnDisconnect.Enabled = false;
                btnSend.Enabled = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if ( tboxUSBVID.Text  == "" )
            {
                MessageBox.Show("Please Enter VID");
            }
            if (tboxUSBVID.TextLength != 4 )
            {
                MessageBox.Show("VID Length Error");
            }

            usbI = new USBInterface("vid_" + tboxUSBVID.Text.ToLower());

            string[] list = usbI.getDeviceList();
            for (int i = 0; i < list.Length; i++)
            {
                lboxList.Items.Add(list[i]);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            usbI.Disconnect();
            btnConnect.Enabled = true;
        }

        private void lboxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            for (i = 0; i < lboxList.Items .Count; i++)
            {
                if ( lboxList.SelectedIndex == i )
                {
                    string curString = lboxList.SelectedItem.ToString();
                    int index = curString.IndexOf("pid_");
                    tboxUSBPID.Text = curString.Substring(index + 4, 4);
                }
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            // send data in hex
            byte[] send = new byte[16];

            send[0] = 0x56;
            send[1] = 0x69;
            send[2] = 0x56;
            send[3] = 0x4F;
            send[4] = 0x74;
            send[5] = 0x65;
            send[6] = 0x63;
            send[7] = 0x68;
            send[8] = 0x32;
            send[9] = 0x00;
            send[10] = 0x18;
            send[11] = 0x01;
            send[12] = 0x00;
            send[13] = 0x00;
            send[14] = 0xB3;
            send[15] = 0xCD;

            usbI.write(send);
            if (USBHIDDRIVER.USBInterface.usbBuffer.Count > 0)
            {
                byte[] receive = null;
                int counter = 0;
                while ((byte[])USBHIDDRIVER.USBInterface.usbBuffer[counter] == null)
                {
                    //Remove this report from list
                    lock (USBHIDDRIVER.USBInterface.usbBuffer.SyncRoot)
                    {
                        USBHIDDRIVER.USBInterface.usbBuffer.RemoveAt(0);
                    }
                }
                //since the remove statement at the end of the loop take the first element
                receive = (byte[])USBHIDDRIVER.USBInterface.usbBuffer[0];
                lock (USBHIDDRIVER.USBInterface.usbBuffer.SyncRoot)
                {
                    USBHIDDRIVER.USBInterface.usbBuffer.RemoveAt(0);
                }

                //DO SOMETHING WITH THE RECORD HERE

            }

            Thread.Sleep(5);
            usbI.startRead();
            Thread.Sleep(5);
            usbI.stopRead();
        }

        public void myEventCacher(object sender, System.EventArgs e)
        {
            //// send data in hex
            //byte[] send = new byte[16];

            //send[0] = 0x56;
            //send[1] = 0x69;
            //send[2] = 0x56;
            //send[3] = 0x4F;
            //send[4] = 0x74;
            //send[5] = 0x65;
            //send[6] = 0x63;
            //send[7] = 0x68;
            //send[8] = 0x32;
            //send[9] = 0x00;
            //send[10] = 0x18;
            //send[11] = 0x01;
            //send[12] = 0x00;
            //send[13] = 0x00;
            //send[14] = 0xB3;
            //send[15] = 0xCD;

            //usbI.write(send);
            //if (USBHIDDRIVER.USBInterface.usbBuffer.Count > 0)
            //{
            //    byte[] receive = null;
            //    int counter = 0;
            //    while ((byte[])USBHIDDRIVER.USBInterface.usbBuffer[counter] == null)
            //    {
            //        //Remove this report from list
            //        lock (USBHIDDRIVER.USBInterface.usbBuffer.SyncRoot)
            //        {
            //            USBHIDDRIVER.USBInterface.usbBuffer.RemoveAt(0);
            //        }
            //    }
            //    //since the remove statement at the end of the loop take the first element
            //    receive = (byte[])USBHIDDRIVER.USBInterface.usbBuffer[0];
            //    lock (USBHIDDRIVER.USBInterface.usbBuffer.SyncRoot)
            //    {
            //        USBHIDDRIVER.USBInterface.usbBuffer.RemoveAt(0);
            //    }

            //    //DO SOMETHING WITH THE RECORD HERE

            //}

            //Thread.Sleep(5);
            //usbI.startRead();
            //Thread.Sleep(5);
            //usbI.stopRead();
        }
    }
}
